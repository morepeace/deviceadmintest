package com.symantec.deviceadmintest;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
    private static final String LOG_TAG = "DeviceAdminTest: " + MainActivity.class.getSimpleName();
    public static final int ADMIN_ENABLE_RESULT_CODE = 3;

    Button enableAdmin;
    TextView adminStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Device Admin Test");

        enableAdmin = findViewById(R.id.button_enable_admin);
        adminStatus = findViewById(R.id.text_admin_status);
       showDeviceAdminStatus();
    }

    @Override
    protected void onResume() {
        super.onResume();
        showDeviceAdminStatus();
    }

    private void showDeviceAdminStatus() {
        if (!isDeviceAdmin()) {
            enableAdmin.setVisibility(View.VISIBLE);
            adminStatus.setText("Device admin not enabled!");
            enableAdmin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Ask for device admin
                    askForDeviceAdmin();
                }
            });
        } else {
            enableAdmin.setVisibility(View.GONE);
            adminStatus.setText("Device admin enabled! To test this issue, try disabling device admin " +
                    "from context menu in app tray using the following methods:" +
                    "1 ) Long press app icon in app tray, select uninstall.  " +
                    "In the Manage Device Administrators screen tap the \"Deactivate\" button. " +
                    "This flow is the same on other Android devices/versions and behaves as expected. " +
                    "This sends an \"android.app.action.DEVICE_ADMIN_DISABLE_REQUESTED\"" +
                    "2)  Press app icon in app tray, select App Info, select uninstall on App Info screen. " +
                    "Again Device admin screen is shown but the button is now \"Deactivate and uninstall\" " +
                    "instead of just \"Deactivate\". This sends a different intent, \"android.app.action.DEVICE_ADMIN_DISABLED\"  ");
        }
    }

    private boolean isDeviceAdmin() {
        ComponentName devAdminReceiver = new ComponentName(this, AdminReceiver.class);
        DevicePolicyManager dpm = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        return dpm.isAdminActive(devAdminReceiver);
    }

    private void askForDeviceAdmin() {
        // Launch the activity to have the user enable device admin.
        Intent intent = new Intent("android.app.action.ADD_DEVICE_ADMIN");
        intent.putExtra("android.app.extra.DEVICE_ADMIN", new ComponentName(MainActivity.this, AdminReceiver.class));
        intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "Please make me the device admin!");
        Log.d(LOG_TAG, "Requesting device admin");
        startActivityForResult(intent, ADMIN_ENABLE_RESULT_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADMIN_ENABLE_RESULT_CODE) {
            if (resultCode == RESULT_CANCELED) {
                Log.d(LOG_TAG, "In onActivityResult: RESULT_CANCELED");
                Toast.makeText(this, "Need to turn on device admin to see this issue", Toast.LENGTH_LONG).show();
            } else if (resultCode == RESULT_OK) {
                    Log.d(LOG_TAG, "In onActivityResult: RESULT_OK");
            }
        }
    }
}
