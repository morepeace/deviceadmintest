package com.symantec.deviceadmintest;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class DeviceAdminOffActivity extends Activity {

    private static final String LOG_TAG = "DeviceAdminTest: " + DeviceAdminOffActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String callingActivity = getIntent().getStringExtra("CallingClass");
        Log.d(LOG_TAG, "Called by:" + callingActivity);

        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setIcon(android.R.drawable.ic_dialog_info);
        b.setTitle("Device Admin Disabled!");
        b.setMessage("Device Admin Disabled!");
        b.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        b.create().show();
    }
}
