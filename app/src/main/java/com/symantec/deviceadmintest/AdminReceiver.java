package com.symantec.deviceadmintest;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Rajeshwari_Godbole on 3/27/18.
 */

public class AdminReceiver extends android.app.admin.DeviceAdminReceiver {
    private static final String LOG_TAG = "DeviceAdminTest: " + AdminReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.d(LOG_TAG, "Intent received: " + action);
        if (action.equalsIgnoreCase(android.app.admin.DeviceAdminReceiver.ACTION_DEVICE_ADMIN_ENABLED)) {
            Intent enabled = new Intent(context, DeviceAdminOnActivity.class);
            enabled.putExtra("CallingClass", "AdminReceiver");
            context.startActivity(enabled);
        } else if (action.equalsIgnoreCase(android.app.admin.DeviceAdminReceiver.ACTION_DEVICE_ADMIN_DISABLED)) {
            try {
                //sleep for 5 secs to see if the app is killed
                Log.d(LOG_TAG,"Initiating a sleep of 10 secs to simulate time taken by the MDM app to cleanup");
                Thread.sleep(10000);
                Log.d(LOG_TAG, "Sleep done!");
            } catch (InterruptedException e) {
                e.printStackTrace();
                Log.e(LOG_TAG, "Sleep interrupted");
            }
            //TODO: Issue seen here: The below activity is never called when the "Deactivate and uninstall" button is tapped
            //TODO: in the app info screen
            Intent disabled = new Intent(context, DeviceAdminOffActivity.class);
            disabled.putExtra("CallingClass", "AdminReceiver");
            Log.d(LOG_TAG, "Starting DeviceAdminOffActivity");
            context.startActivity(disabled);
        } else if (action.equalsIgnoreCase(android.app.admin.DeviceAdminReceiver.ACTION_DEVICE_ADMIN_DISABLE_REQUESTED)) {
            //This flow behaves correctly as it has in past Android versions
            Intent disableRequested = new Intent(context, DeviceAdminDisableRequestedActivity.class);
            disableRequested.putExtra("CallingClass", "AdminReceiver");
            Log.d(LOG_TAG, "Starting DeviceAdminDisableRequestedActivity");
            context.startActivity(disableRequested);
        }
    }
}
