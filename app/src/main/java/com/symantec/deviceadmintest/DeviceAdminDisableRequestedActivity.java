package com.symantec.deviceadmintest;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

public class DeviceAdminDisableRequestedActivity extends Activity {

    private static final String LOG_TAG = "DeviceAdminTest: " + DeviceAdminDisableRequestedActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(LOG_TAG, "In onCreate for DeviceAdminDisableRequestedActivity");

        String callingActivity = getIntent().getStringExtra("CallingClass");
        Log.d(LOG_TAG, "Called by:" + callingActivity);

        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setIcon(android.R.drawable.ic_dialog_info);
        b.setTitle("Device Admin Disable Requested!");
        b.setMessage("Device Admin Disabled Requested. Cleaning up policies and app data!");
        b.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d(LOG_TAG, "App has now finished the cleanup it needed to do and can now exit");
                callDeviceAdminOffActivity();
                finish();
            }
        });
        b.create().show();

    }

    private void callDeviceAdminOffActivity() {
        Intent intent = new Intent(this, DeviceAdminOffActivity.class);
        intent.putExtra("CallingClass", "DeviceAdminDisableRequestedActivity");
        startActivity(intent);
    }
}
