package com.symantec.deviceadmintest;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

public class DeviceAdminOnActivity extends Activity {

    private static final String LOG_TAG = "DeviceAdminTest: " + DeviceAdminOnActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setIcon(android.R.drawable.ic_dialog_info);
        b.setTitle("Device Admin Enabled!");
        b.setMessage("Device Admin Enabled!");
        b.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        b.create().show();

        String callingActivity = getIntent().getStringExtra("CallingClass");
        Log.d(LOG_TAG, "Called by:" + callingActivity);
    }
}
